<?php declare(strict_types=1);

namespace Plugin\jtl_sort_avail;

use JTL\Filter\ProductFilter;
use JTL\Filter\SortingOptions\AbstractSortingOption;
use JTL\Shop;

/**+
 * Class SortAvail
 * @package Plugin\jtl_sort_avail
 */
class SortAvail extends AbstractSortingOption
{
    /**
     * SortAvail constructor.
     * @param ProductFilter $productFilter
     */
    public function __construct(ProductFilter $productFilter)
    {
        parent::__construct($productFilter);
        $this->orderBy = 'tartikel.fLagerbestand DESC, tartikel.cLagerKleinerNull DESC, tartikel.cName';
        $this->setName(Shop::Lang()->get('filterAvailability'));
        $this->setPriority(19);
        $this->setValue(98);
    }
}
