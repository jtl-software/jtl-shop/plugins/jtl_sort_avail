<?php declare(strict_types=1);

namespace Plugin\jtl_sort_avail;

use JTL\Events\Dispatcher;
use JTL\Filter\SortingOptions\Factory;
use JTL\Plugin\Bootstrapper;

/**
 * Class Bootstrap
 * @package Plugin\jtl_sort_avail
 */
class Bootstrap extends Bootstrapper
{
    /**
     * @param Dispatcher $dispatcher
     */
    public function boot(Dispatcher $dispatcher): void
    {
        parent::boot($dispatcher);
        $dispatcher->listen('shop.hook.' . \HOOK_PRODUCTFILTER_REGISTER_SEARCH_OPTION, [$this, 'hook254']);
        if ($this->getPlugin()->getConfig()->getValue('is_default') === 'Y') {
            $dispatcher->listen('shop.hook.' . \HOOK_PRODUCTFILTER_INIT, [$this, 'hook250']);
        }
    }

    /**
     * @param array $args
     */
    public function hook250(array $args): void
    {
        if (!isset($_SESSION['Usersortierung']) || $_SESSION['Usersortierung'] === \SEARCH_SORT_STANDARD) {
            $_GET['Sortierung'] = 98;
        }
    }

    /**
     * @param array $args
     */
    public function hook254(array $args): void
    {
        $factory = $args['factory'];
        /** @var Factory $factory */
        $factory->registerSortingOption(98, SortAvail::class);
    }
}
